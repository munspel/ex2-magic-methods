<?php

trait CalculatorTrait {
    public function sum($a, $b){
        return $a+$b;
    }
}

class TestClass1{
    // ...
    use CalculatorTrait;
}
class TestClass2{
     // ...
    use CalculatorTrait;
}
