<?php

/**
 * Interface ILogger
 * @
 */
interface ILogger
{
    public function log($message);

    public function log_error($message);
}

class FileLogger implements ILogger
{
    /**
     * @var string
     */
    protected $context;
    protected $data = "test";

    private function __construct()
    {
        $this->createContext();
        echo "<br>File was opened!<br>";
    }

    /**
     * @return FileLogger
     */
    public static function make()
    {
        return new self();
    }

    /**
     * Create or return file descriptor
     * @return false|resource
     */
    protected function getContext()
    {

        return $this->context;
    }

    /**
     * Closes file
     */
    protected function closeContext()
    {
        if ($this->context) {
            fclose($this->context);
            $this->context = null;
            echo "<br>File was closed!<br>";
        }
    }

    public function __destruct()
    {
        $this->closeContext();
    }

    /**
     * @param $message
     * @return string
     * @throws Exception
     */
    protected function formatInfoMessage($message)
    {
        return (new DateTime)->format("Y-m-d H:i:s:v") . " Info: " . $message . "\n";
    }

    /**
     * @param $message
     * @return string
     * @throws Exception
     */
    protected function formatErrorMessage($message)
    {
        return (new DateTime)->format("Y-m-d H:i:s:v") . " Error: " . $message . "\n";
    }

    public function __sleep()
    {
        echo "sleep<br>";
        $this->closeContext();
        return ['data'];
    }
    public function __wakeup()
    {

        echo "wakeup<br>";
        $this->createContext();
    }

    /**
     * @param $message
     */
    public function log_error($message)
    {
        $context = $this->getContext();
        fwrite($context, $this->formatErrorMessage($message));
    }

    /**
     * @param $message
     */
    public function log($message)
    {
        $context = $this->getContext();
        fwrite($context, $this->formatInfoMessage($message));
    }

    protected function createContext()
    {
        $fname = "../data/" . date("Y_m_d") . '_log.txt';
        $this->context = fopen($fname, 'a');
        echo "<br>File was created!<br>";
    }
}

/**
 * Shows different types of magic methods
 * Class BaseComponent
 */
class BaseComponent
{

    protected $_data;

    /**
     * @param $name
     * @return |null
     */
    public function __get($name)
    {
        return $this->_data[$name] ?? null;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->_data[$name] = $value;
    }

    /**
     * @param $name
     * @param $arguments
     * @return BaseComponent
     */
    public static function __callStatic($name, $arguments)
    {
        if ($name == "make") {
            return new self();
        }
    }

    /**
     * @param $name
     * @param $arguments
     * @return $this|null
     */
    public function __call($name, $arguments)
    {
        $re = '/get(.*)/m';
        preg_match_all($re, $name, $matches, PREG_SET_ORDER, 0);
        if ($matches) {
            return $this->_data[strtolower($matches[0][1])] ?? null;
        }

        $re = '/set(.*)/m';
        preg_match_all($re, $name, $matches, PREG_SET_ORDER, 0);
        if ($matches) {
            $this->_data[strtolower($matches[0][1])] = $arguments[0];
            return $this;
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return $this->_data;
    }
}



