<?php
require_once "../src/magic_tests.php";
require_once "../src/traits.php";
spl_autoload();


/*
// Magic method examples
$obj = BaseComponent::make();
$obj->name = "Hello";
$obj->setName("Vasy")
    ->setAge(33)
    ->setStatus("free");
print_r($obj->toArray());

// Logger tests
$logger = FileLogger::make();
$logger->log("First mmessage");
$logger->log_error("Error message");
echo "Logger will be serialized now:<br>";
$sLogger = serialize($logger);
$logger = unserialize($sLogger);
echo "Logger was unerialized<br>";
$logger->log("Message after serialization");
*/
$obj1 = new TestClass1();
$obj2 = new TestClass2();
echo $obj1->sum(1,2);
echo $obj2->sum(4,2);
